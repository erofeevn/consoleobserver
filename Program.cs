﻿using System;
using ConsoleObserver.Scenarios;

namespace ConsoleObserver
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Press 1 for iOs, 2 for Android");
            char ch = Console.ReadKey().KeyChar;Console.WriteLine();
            switch(ch)
            {
                case '1':
                    Scenarios.iOsScenario iOs = new Scenarios.iOsScenario();
                    iOs.Run();
                break;
                case'2':
                    Scenarios.AndroidScenario android = new Scenarios.AndroidScenario();
                    android.Run();
                break;
                default:
                return;
            }
            while (true)
            {
                char input;
                input = Console.ReadKey().KeyChar;
                switch (input)
                {
                    case 'q':
                        return;
                    default:
                        break;
                }
            }
        }
    }
}
