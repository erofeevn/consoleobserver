﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Text.RegularExpressions;
using ConsoleObserver;

namespace ConsoleObserver
{
    [DataContract(Name = "Config")]
    public class Config
    {
        public static string AndroidConfigFileName = "config_android.xml";
        public static string IosConfigFileName = "config_ios.xml";
        [DataMember (Name = "ExecutionFileName", Order = 1)]
        public string ExecutionFileName { get; set; }
        [DataMember (Name = "Arguments", Order = 2)]
        public string Arguments { get; set; }
        [DataMember (Name = "UseStringContainsForRules", Order = 3)]
        public bool UseStringContainsForRules {get;set;}
        [DataMember (Name = "SaveFullOutput", Order = 4)]
        public bool SaveFullOutput { get; set; }
        [DataMember (Name = "Rules", Order = 5)]
        public List<Rule> Rules { get; set; }

        public static void Serialize(Config config)
        {
            var ds = new DataContractSerializer(typeof(Config));
            using (Stream s = File.Create(AndroidConfigFileName))
                ds.WriteObject(s, config);
        }

        public static Config Deserialize(string fileName)
        {
            Config config;
            var ds = new DataContractSerializer(typeof(Config));
            using (Stream s = File.OpenRead(fileName))
                config = (Config)ds.ReadObject(s);
            return config;
        }
    }
    [DataContract(Name = "Rule")]
    public class Rule
    {
        [DataMember (Name = "Message", IsRequired = true)]
        public string Message { get; set; }
        [DataMember (Name = "MessageRule", IsRequired = true)]
        public string MessageRule { get; set; }
        [DataMember (Name = "MessageColor", IsRequired = true)]
        public int MessageColor { get; set; }
        [DataMember (Name = "RegularExpressionRule", IsRequired = true)]
        public string RegularExpressionRule { get; set; }
        
        internal Regex RegularExpression { get; set; }
        internal Regex RegularExpressionForMessage { get; set; }
    }
}
