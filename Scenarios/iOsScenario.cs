using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
namespace ConsoleObserver.Scenarios
{
    public class iOsScenario
    {
        string _udid;
        public void Run()
        {
            Console.WriteLine("Getting UDID...");
            Config udidConf = new Config();
            udidConf.ExecutionFileName = "ideviceinfo";
            udidConf.Arguments = "";
            udidConf.Rules = new List<Rule>(){new Rule(){
                RegularExpressionRule = ".*UniqueDeviceID.*",
                MessageRule = @"[0-9a-z]{40}"
                }};
            Observer ob = new Observer(udidConf);
            _udid = ob.ObserverAndReturnFirstResult();
            if(string.IsNullOrEmpty(_udid))
            {
                Console.WriteLine("Cant get udid from ideviceinfo");
                return;
            }
            Console.WriteLine($"Success UDID: {_udid}");
            Console.WriteLine("Reading config...");
            Config cf = Config.Deserialize(Config.IosConfigFileName);
            cf.Arguments = $"-u {_udid}";
            Console.WriteLine($"Success. {cf.ExecutionFileName} {cf.Arguments}, {cf.Rules.Count} rules.");
            Console.WriteLine("Start observing...");
            Observer o = new Observer(cf);
            o.Observe();
        }

    }
}