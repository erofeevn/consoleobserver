using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Diagnostics;
namespace ConsoleObserver.Scenarios
{
    public class AndroidScenario
    {
        public void Run()
        {
            Console.WriteLine("Reading config...");
            Config cf = Config.Deserialize(Config.AndroidConfigFileName);
            Console.WriteLine($"Success. {cf.ExecutionFileName} {cf.Arguments}, {cf.Rules.Count} rules.");
            Console.WriteLine("Run adb logcat -c");
            Process clearProcess = new Process();
            clearProcess.StartInfo.FileName = "adb";
            clearProcess.StartInfo.Arguments = "logcat -c";
            clearProcess.StartInfo.UseShellExecute = false;
            clearProcess.StartInfo.RedirectStandardOutput = true;
            clearProcess.Start();
            Console.WriteLine("Done.");
            Console.WriteLine("Start observing...");
            Observer o = new Observer(cf);
            o.Observe();
        }

    }
}