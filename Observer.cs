﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Threading;


namespace ConsoleObserver
{
    public class Observer
    {
        private Task _observingTask { get; set; }

        Config _config;
        string _fileName;
        string _arguments;
        OutputManager _output;
        public Observer(Config config)
        {
            _config = config;
            if (_config.Rules.Count > 0)
            {
                foreach (var item in _config.Rules)
                {
                    item.RegularExpression = new Regex(item.RegularExpressionRule, RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
                    item.RegularExpressionForMessage = new Regex(item.MessageRule, RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
                }
            }
        }
        public string ObserverAndReturnFirstResult()
        {
            Process observedProcess = new Process();
            observedProcess.StartInfo.FileName = _config.ExecutionFileName;
            observedProcess.StartInfo.Arguments = _config.Arguments;
            observedProcess.StartInfo.UseShellExecute = false;
            observedProcess.StartInfo.RedirectStandardOutput = true;
            observedProcess.Start();
            while (true)
            {
                string str = observedProcess.StandardOutput.ReadLine();
                if(str == null)
                {
                    return "";
                }
                foreach (var rule in _config.Rules)
                {
                    if (rule.RegularExpression.IsMatch(str))
                    {
                        Match matchForMessage = rule.RegularExpressionForMessage.Match(str);
                        if (matchForMessage.Success)
                        {
                            return matchForMessage.Value;
                        }
                    }
                }
            }
        }
        public void Observe()
        {
            Process observedProcess = new Process();
            observedProcess.StartInfo.FileName = _config.ExecutionFileName;
            observedProcess.StartInfo.Arguments = _config.Arguments;
            observedProcess.StartInfo.UseShellExecute = false;
            observedProcess.StartInfo.RedirectStandardOutput = true;
            _observingTask = new Task(() =>
            {
                if(_config.SaveFullOutput)
                    _output = new OutputManager();
                observedProcess.Start();
                while (true)
                {
                    string str = observedProcess.StandardOutput.ReadLine();
                    if(str == null)
                    {
                        Console.WriteLine("Observing is end. Press q for exit.");
                        _output.Dispose();
                        return;
                    }
                    if(string.IsNullOrEmpty(str))
                        continue;
                    _output?.WriteLine(str);
                    foreach (var rule in _config.Rules)
                    {
                        if((_config.UseStringContainsForRules && str.Contains(rule.RegularExpressionRule)) ||
                          (!_config.UseStringContainsForRules && rule.RegularExpression.IsMatch(str))) 
                        {
                            Console.ForegroundColor = (ConsoleColor)rule.MessageColor;
                            parseAndWriteDate(str);
                            Console.Write(rule.Message + "\t");
                            Match matchForMessage = rule.RegularExpressionForMessage.Match(str);
                            if (matchForMessage.Success)
                            {
                                Console.WriteLine(matchForMessage.Value);
                            }
                            Console.ForegroundColor = ConsoleColor.White;
                        }
                    }
                }
            });
            _observingTask.Start();
        }
        Regex _dateRegex = new Regex(@"[0-9\-]{5} [0-9\:\.]{8,12}", RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.Singleline);
        void parseAndWriteDate(string str)
        {
            var match = _dateRegex.Match(str);
            if(match.Success)
                Console.Write("T" + match.Value + "\t");
            else
                Console.Write("F" + DateTime.Now.ToString("HH:mm:ss.fff") + "\t");
        }
    }
}
