using System;
using System.IO;
namespace ConsoleObserver
{
    public class OutputManager:IDisposable
    {
        DirectoryInfo _directoryInfo;
        const int MaxLinesInFile = 5000;
        FileStream _fs;
        TextWriter _wr;
        int Counter;
        public OutputManager(string folderName = "Output")
        {
            _directoryInfo = Directory.CreateDirectory(folderName);
            _fs = File.Create(_directoryInfo.FullName + "\\" + DateTime.Now.ToString("d-MM HH-mm-ss"));
            _wr = new StreamWriter(_fs);
        }
        ~OutputManager()
        {
            if (_fs != null)
                _fs.Close();
        }
        public void WriteLine(string line)
        {
            _wr.WriteLine(line);
            Counter++;
            if(Counter > MaxLinesInFile)
            {
                openNewFile();
                Counter = 0;
            }
        }
        private void openNewFile()
        {
            _wr.Close();
            _fs.Close();
            _fs = File.Create(_directoryInfo.FullName + "\\" + DateTime.Now.ToString("d-MM HH-mm-ss"));
            _wr = new StreamWriter(_fs);
        }
        public void Dispose()
        {
            if (_fs != null)
                _fs.Close();
        }
    }
}